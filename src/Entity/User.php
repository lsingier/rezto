<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ID_Cli;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom_Cli;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Prenom_Cli;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Tel_Cli;

    /**
     * @ORM\Column(type="integer")
     */
    private $Age_Cli;

    /**
     * @ORM\Column(type="array")
     */
    private $Preferences_Cli = [];

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $LieuPlusVisite;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIDCli(): ?string
    {
        return $this->ID_Cli;
    }

    public function setIDCli(string $ID_Cli): self
    {
        $this->ID_Cli = $ID_Cli;

        return $this;
    }

    public function getNomCli(): ?string
    {
        return $this->Nom_Cli;
    }

    public function setNomCli(string $Nom_Cli): self
    {
        $this->Nom_Cli = $Nom_Cli;

        return $this;
    }

    public function getPrenomCli(): ?string
    {
        return $this->Prenom_Cli;
    }

    public function setPrenomCli(string $Prenom_Cli): self
    {
        $this->Prenom_Cli = $Prenom_Cli;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->Email;
    }

    public function setEmail(string $Email): self
    {
        $this->Email = $Email;

        return $this;
    }

    public function getTelCli(): ?string
    {
        return $this->Tel_Cli;
    }

    public function setTelCli(string $Tel_Cli): self
    {
        $this->Tel_Cli = $Tel_Cli;

        return $this;
    }

    public function getAgeCli(): ?int
    {
        return $this->Age_Cli;
    }

    public function setAgeCli(int $Age_Cli): self
    {
        $this->Age_Cli = $Age_Cli;

        return $this;
    }

    public function getPreferencesCli(): ?array
    {
        return $this->Preferences_Cli;
    }

    public function setPreferencesCli(array $Preferences_Cli): self
    {
        $this->Preferences_Cli = $Preferences_Cli;

        return $this;
    }

    public function getLieuPlusVisite(): ?string
    {
        return $this->LieuPlusVisite;
    }

    public function setLieuPlusVisite(string $LieuPlusVisite): self
    {
        $this->LieuPlusVisite = $LieuPlusVisite;

        return $this;
    }
}
