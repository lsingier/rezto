<?php
 
namespace App\Tests;
 
use App\Entity\User;
use PHPUnit\Framework\TestCase;
 
class UnitTest extends TestCase
{
    public function testDemo()
    {
        $user = new User();
 
        $user->setNomCli('demo');
 
        $this->assertTrue($user->getNomCli() === 'demo');
    }
}