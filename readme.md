# Foobar

Projet de réservation de table et de commandes pour bar et restaurant.
Créer par Lucas SINGIER
## Installation

Install project in CMD

```bash
git clone https://gitlab.com/lsingier/rezto.git
```

## License
[MIT](https://choosealicense.com/licenses/mit/)